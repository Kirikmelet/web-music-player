import express from 'express'
import * as path from 'path'
import { fileURLToPath } from 'url'

const __dirname = path.dirname(fileURLToPath(import.meta.url))
const app = express()

app.get('/',  (_req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'))
})

app.use(express.static(path.join(__dirname)))


app.listen(5438, () => { console.log('PORT: 5438') })
