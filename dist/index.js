"use strict";
window.addEventListener('load', () => {
    (async () => {
        console.log('I NEED MUSIC NOW!');
        const input_v = document.getElementById('video_input') ?? void 0;
        const video = document.getElementById('video_player') ?? void 0;
        MediaControlLink.staticLink(input_v, video);
    })();
});
class MediaControlLink {
    static staticLink(inputEl, outputEl) {
        outputEl.addEventListener('ended', () => {
            URL.revokeObjectURL(outputEl.src);
        });
        inputEl.addEventListener('change', () => {
            if (inputEl.files) {
                outputEl.src = URL.createObjectURL(inputEl.files[0]);
                if (!outputEl.classList.contains('hasSrc')) {
                    outputEl.classList.add('hasSrc');
                }
                console.debug(inputEl.classList);
            }
        });
        const inputClearEl = document.getElementById(`${inputEl.id}_clear`);
        if (inputClearEl) {
            inputClearEl.addEventListener('click', () => {
                inputEl.src = '';
                URL.revokeObjectURL(outputEl.src);
            });
        }
    }
}
