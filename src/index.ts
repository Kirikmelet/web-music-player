window.addEventListener('load', () => {
    (async () => {
        console.log('I NEED MUSIC NOW!')
        const input_v: HTMLInputElement = <HTMLInputElement>document.getElementById('video_input') ?? void 0
        const video: HTMLVideoElement = <HTMLVideoElement>document.getElementById('video_player') ?? void 0
        MediaControlLink.staticLink(input_v, video)
    })()
})

class MediaControlLink {
    static staticLink(inputEl: HTMLInputElement, outputEl: HTMLAudioElement|HTMLVideoElement) {
        outputEl.addEventListener('ended', () => {
            URL.revokeObjectURL(outputEl.src);
        });
        inputEl.addEventListener('change', () => {
            if (inputEl.files) {
                outputEl.src = URL.createObjectURL(inputEl.files[0]);
                if (!outputEl.classList.contains('hasSrc')) {
                  outputEl.classList.add('hasSrc')
                }
                console.debug(inputEl.classList)
            }
        });
        const inputClearEl = document.getElementById(`${inputEl.id}_clear`)
        if (inputClearEl) {
          inputClearEl.addEventListener('click', () => {
            inputEl.src = ''
            URL.revokeObjectURL(outputEl.src)
          })
        }
    }
}
